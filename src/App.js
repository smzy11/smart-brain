import React, { Component } from "react";
import "./App.css";
// import Particles from "react-particles-js";
import Navigation from "./components/Navigation/Navigation";
import Singin from "./components/Singin/Singin";
import Register from "./components/Register/Register";
// import Logo from "./components/Logo/Logo";
import ImageLinkForm from "./components/ImageLinkForm/ImageLinkForm";
import Rank from "./components/Rank/Rank";

// const particlesParams = {
//   particles: {
//     number: {
//       value: 30,
//       density: {
//         enable: true,
//         value_area: 800,
//       },
//     },
//   },
// };

const initialState = {
  input: "",
  route: "singin",
  isSingedIn: false,
  user: {
    id: "",
    name: "",
    email: "",
    entries: 0,
    joined: "",
  },
};

class App extends Component {
  constructor() {
    super();
    this.state = initialState;
  }

  onInputChange = (event) => {
    console.log(event.target.value);
  };
  onButtonClick = () => {
    console.log("click");
  };
  onRouteChange = (route) => {
    if (route === "signout") {
      this.setState(initialState);
    } else if (route === "home") {
      this.setState({ isSingedIn: true });
    }
    this.setState({ route: route });
  };

  loadUser = (data) => {
    this.setState({
      user: {
        id: data.id,
        name: data.name,
        email: data.email,
        entries: data.entries,
        joined: data.joined,
      },
    });
  };

  render() {
    const { isSingedIn, route } = this.state;
    return (
      <div className="App">
        {/* <Particles className="particles" params={particlesParams} /> */}
        <Navigation
          onRouteChange={this.onRouteChange}
          isSingedIn={isSingedIn}
        />
        {route === "home" ? (
          <div>
            {/* <Logo /> */}
            <Rank
              name={this.state.user.name}
              entries={this.state.user.entries}
            />
            <ImageLinkForm
              onInputChange={this.onInputChange}
              onButtonClick={this.onButtonClick}
            />
          </div>
        ) : route === "singin" ? (
          <Singin loadUser={this.loadUser} onRouteChange={this.onRouteChange} />
        ) : (
          <Register
            loadUser={this.loadUser}
            onRouteChange={this.onRouteChange}
          />
        )}
      </div>
    );
  }
}

export default App;
